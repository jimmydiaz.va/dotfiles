# enable proper true color functionality 
set -g default-terminal "screen-256color"

# change prefix 
set -g prefix C-a
unbind C-b
bind-key C-a send-prefix 

# split pane horizontally 
unbind % 
bind | split-window -h 

# split pane vertically 
unbind '"'
bind - split-window -v 

# reload tmux.conf 
unbind r 
bind r source-file ~/.config/tmux/tmux.conf

# Resizing panes 
bind -r j resize-pane -D 5
bind -r k resize-pane -U 5
bind -r l resize-pane -R 5
bind -r h resize-pane -L 5

# maximize/minimize panes
bind -r m resize-pane -Z 

# enable mouse support for resize-pane
set -g mouse on

# enable vim movements 
set-window-option -g mode-keys vi 

# enable copy mode 
bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -T copy-mode-vi 'y' send -X copy-selection

# enable mouse scrolling and copy 
unbind -T copy-mode-vi MouseDragEnd1Pane

# plugin manager 
set -g @plugin 'tmux-plugins/tpm'

# list of tmux plugins 
set -g @plugin 'christoomey/vim-tmux-navigator' # navigate between splits 
set -g @plugin 'jimeh/tmux-themepack'
set -g @plugin 'tmux-plugins/tmux-resurrect' # persist tmux sessions after computer restart 
set -g @plugin 'tmux-plugins/tmux-continuum' # automatically saves sessions for you every 15 minutes 

# source the theme file
source-file '/home/lyf4z/.config/tmux/plugins/tmux-themepack/powerline/default/cyan.tmuxtheme'

set -g @resurrect-capture-pane-contents 'on' 
set -g @continuum-restore 'on'

# initialize tmux plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.config/tmux/plugins/tpm/tpm'
