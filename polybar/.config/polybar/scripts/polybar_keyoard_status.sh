#!/bin/bash

# Check the click type and perform actions accordingly
if [ "$1" == "left" ]; then
  # Show a YAD message on left click
  yad --text "Keyboard Actions" --button=gtk-ok
elif [ "$1" == "right" ]; then
  # Run your custom script on right click
  ~/path/to/your_script.sh
fi

# Output the icon (for Polybar)
echo ""
