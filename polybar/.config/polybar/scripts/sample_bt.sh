#!/bin/bash

# NOTE: 
# Script: Bluetooth Status and Battery Indicator
# Author: lyf4z
# Description: This script checks if a specified Bluetooth device is connected and displays the device name 
#              and battery percentage, with color warnings based on battery level. 
#              It also handles cases where the device is disconnected or battery information is unavailable.

# Function to get the MAC address of a device by its alias
get_mac_by_alias() {
    local alias=$1
    bluetoothctl devices | grep "$alias" | awk '{print $2}'
}

DEVICE_ALIAS_1="Bluetooth Music"
DEVICE_ALIAS_2="mami_dual"
DEVICE_ALIAS_3="OWS"

# Retry function to handle detection delay
retry_device_info() {
    local alias=$1
    local retries=5
    local sleep_duration=2

    for ((i = 0; i < retries; i++)); do
        MAC=$(get_mac_by_alias "$alias")
        if [ -n "$MAC" ]; then
            echo "$MAC"
            return 0
        fi
        sleep "$sleep_duration"
    done
    return 1
}

# Check for the first device
DEVICE_MAC_1=$(retry_device_info "$DEVICE_ALIAS_1")

# Check for the second device
DEVICE_MAC_2=$(retry_device_info "$DEVICE_ALIAS_2")

# Check for the third device
DEVICE_MAC_3=$(retry_device_info "$DEVICE_ALIAS_3")

# Function to get the device info
get_device_info() {
    local MAC=$1
    local NAME=$2

    BATTERY_PERCENTAGE=$(bluetoothctl info "$MAC" | grep -i "Battery Percentage" | grep -oP '\(\K[0-9]+')

    if [ -z "$BATTERY_PERCENTAGE" ]; then
        echo "%{F#0000FF}󰂰%{F-} $NAME"
    else
        if [ "$BATTERY_PERCENTAGE" -le 10 ]; then
            echo "%{F#FF0000}󰂰 %{F-}$BATTERY_PERCENTAGE% $NAME"
        elif [ "$BATTERY_PERCENTAGE" -le 25 ]; then
            echo "%{F#FFA500}󰂰 %{F-}$BATTERY_PERCENTAGE% $NAME"
        else
            echo "%{F#0000FF}󰂰 %{F-}$BATTERY_PERCENTAGE% $NAME"
        fi
    fi
}

# Function to check if bluetoothctl is running
check_bluetooth_service() {
    if systemctl is-active --quiet bluetooth.service; then
        echo "%{F#00FF00}󰂲%{F-} "  # Green icon when Bluetooth is running
    else
        echo "%{F#FF0000}󰂲%{F-} "  # Red icon when Bluetooth is not running
    fi
}

# Print the service status icon first
check_bluetooth_service

# Check if the first device is connected
if [ -n "$DEVICE_MAC_1" ] && bluetoothctl info "$DEVICE_MAC_1" | grep -q "Connected: yes"; then
    get_device_info "$DEVICE_MAC_1" "$DEVICE_ALIAS_1"
elif [ -n "$DEVICE_MAC_2" ] && bluetoothctl info "$DEVICE_MAC_2" | grep -q "Connected: yes"; then
    get_device_info "$DEVICE_MAC_2" "$DEVICE_ALIAS_2"
elif [ -n "$DEVICE_MAC_3" ] && bluetoothctl info "$DEVICE_MAC_3" | grep -q "Connected: yes"; then
    get_device_info "$DEVICE_MAC_3" "$DEVICE_ALIAS_3"
else
    echo "󰂲"  # Disconnected icon if neither device is connected
fi
