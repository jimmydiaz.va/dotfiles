#!/bin/bash

# NOTE: 
# Script: Bluetooth Status and Battery Indicator
# Author: lyf4z
# Description: This script checks if a specified Bluetooth device is connected and displays the device name 
#              and battery percentage, with color warnings based on battery level. 
#              It also handles cases where the device is disconnected or battery information is unavailable.

# Define the MAC addresses of the devices
DEVICE_MAC_1="41:42:DC:2B:E8:CE"  # Bluetooth Music
DEVICE_MAC_2="41:42:5A:01:BE:E0"  # OWS

# Function to get the device info and battery percentage
get_device_info() {
    local MAC=$1
    local NAME=$2

    # Check if the device is connected
    if bluetoothctl info "$MAC" | grep -q "Connected: yes"; then
        BATTERY_PERCENTAGE=$(bluetoothctl info "$MAC" | grep -i "Battery Percentage" | grep -oP '\(\K[0-9]+')
        
        # Handle Bletooth Music name as "BM"
        if [ "$NAME" = "Bluetooth Music" ]; then
            NAME="BM"
        fi

        if [ -z "$BATTERY_PERCENTAGE" ]; then
            echo "%{F#0000FF}󰂰 %{F-}%{F#FFFF00}$NAME%{F-} (Battery info unavailable)"
        else
            if [ "$BATTERY_PERCENTAGE" -le 10 ]; then
                echo "%{F#FF0000}󰂰 %{F-}$BATTERY_PERCENTAGE% %{F#FFFF00}$NAME%{F-}"
            elif [ "$BATTERY_PERCENTAGE" -le 25 ]; then
                echo "%{F#FFA500}󰂰 %{F-}$BATTERY_PERCENTAGE% %{F#FFFF00}$NAME%{F-}"
            else
                echo "%{F#0000FF}󰂰 %{F-}$BATTERY_PERCENTAGE% %{F#FFFF00}$NAME%{F-}"
            fi
        fi
    else
        echo "%{F#FF0000}󰂲 %{F-}"
    fi
}

# Check and display info 
if bluetoothctl info "$DEVICE_MAC_1" | grep -q "Connected: yes"; then
    get_device_info "$DEVICE_MAC_1" "Bluetooth Music"
elif bluetoothctl info "$DEVICE_MAC_2" | grep -q "Connected: yes"; then
    get_device_info "$DEVICE_MAC_2" "OWS"
else
    echo "%{F#FF0000}󰂲 %{F-}"
fi

