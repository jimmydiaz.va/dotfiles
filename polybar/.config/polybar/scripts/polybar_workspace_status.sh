#!/bin/bash

# Define workspace names
all_workspaces=("DEV" "WWW" "VID" "DOC" "VBOX" "CHAT" "MUS" "SYS" "VLOG")

# Define colors
active_color="#FFFFFF"  # White for active workspace with windows
inactive_color="#555555"  # Gray for empty active workspace
count_color="#00FF00"  # Green for window count

# Get the current workspace index
current_workspace_index=$(wmctrl -d | awk '/\* / {print $1}')

# Retrieve the workspace name based on the current index
current_workspace_name="${all_workspaces[$current_workspace_index]}"

# Get list of windows on the current workspace
windows=$(wmctrl -l | awk -v ws="$current_workspace_index" '$2 == ws {print $0}')

# Count the number of windows
window_count=$(echo "$windows" | wc -l)

# Check if there are any windows on the current workspace
if [ "$window_count" -gt 0 ]; then
  # Workspace has windows, show it in active color and window count in green
  final_output="%{F$active_color}$current_workspace_name:%{F$count_color}$window_count%{F-}"
else
  # Workspace has no windows, show it in inactive color and window count in green
  final_output="%{F$inactive_color}$current_workspace_name:%{F$count_color}$window_count%{F-}"
fi

# Output the result
echo "$final_output"
