-- init.lua or your main configuration file

return {
  -- TokyoNight theme with custom configuration
  {
    "folke/tokyonight.nvim",
    priority = 1000,
    config = function()
      local transparent = false -- set to true if you would like to enable transparency

      local bg = "#011628"
      local bg_dark = "#011423"
      local bg_highlight = "#143652"
      local bg_search = "#0A64AC"
      local bg_visual = "#275378"
      local fg = "#CBE0F0"
      local fg_dark = "#B4D0E9"
      local fg_gutter = "#627E97"
      local border = "#547998"

      require("tokyonight").setup({
        style = "night",
        transparent = transparent,
        styles = {
          sidebars = transparent and "transparent" or "dark",
          floats = transparent and "transparent" or "dark",
        },
        on_colors = function(colors)
          colors.bg = bg
          colors.bg_dark = transparent and colors.none or bg_dark
          colors.bg_float = transparent and colors.none or bg_dark
          colors.bg_highlight = bg_highlight
          colors.bg_popup = bg_dark
          colors.bg_search = bg_search
          colors.bg_sidebar = transparent and colors.none or bg_dark
          colors.bg_statusline = transparent and colors.none or bg_dark
          colors.bg_visual = bg_visual
          colors.border = border
          colors.fg = fg
          colors.fg_dark = fg_dark
          colors.fg_float = fg
          colors.fg_gutter = fg_gutter
          colors.fg_sidebar = fg_dark
        end,
      })

      vim.cmd("colorscheme tokyonight")
    end,
  },

  -- Catppuccin theme with custom configuration
  {
    "catppuccin/nvim",
    name = "catppuccin",
    priority = 1000,
    config = function()
      require("catppuccin").setup({
        flavour = "macchiato", -- Choose your preferred variant
        background = { -- Change background settings
          light = "latte",
          dark = "macchiato",
        },
        -- Add more configuration options here
      })
      vim.cmd("colorscheme catppuccin")
    end,

    -- Gruvbox theme with custom configuration
    {
      "ellisonleao/gruvbox.nvim",
      priority = 1000,
      config = true,
      opts = ...,
    },
    -- OneDark theme with custom configuration
    {
      "navarasu/onedark.nvim",
      priority = 1000,
      config = function()
        require("onedark").setup({
          style = "dark", -- Options: "dark", "darker", "cool", "warmer"
          -- Add more configuration options here if needed
        })
        vim.cmd("colorscheme onedark")
      end,
    },

    -- Nightfox theme with custom configuration
    {
      "EdenEast/nightfox.nvim",
      priority = 1000,
      config = function()
        require("nightfox").setup({
          fox = "nightfox", -- Choose from "nightfox", "dayfox", "duskfox", "nordfox"
          -- Add more configuration options here if needed
        })
        vim.cmd("colorscheme nightfox")
      end,
    },
  },
}
