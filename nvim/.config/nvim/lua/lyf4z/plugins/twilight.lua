return {
  -- Dims inactive portions of code
  "folke/twilight.nvim",
  config = function()
    local _twilight = require("twilight")

    -- configure twilight
    _twilight.setup({
      context = 12,
      treesitter = true,
      expand = {
        "function",
        "method",
        "table",
        "if_statement",
      },
    })
  end,
}
