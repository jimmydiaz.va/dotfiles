return { -- sends notification
  "rcarriga/nvim-notify",
  -- event = { "BufReadPre", "BufNewFile" },
  config = function()
    local _notify = require("notify")

    _notify.setup({
      -- additional configs
      timeout = 5000,
      stages = "fade_in_slide_out",
      render = "compact",
    })

    vim.notify = _notify
  end,
}
