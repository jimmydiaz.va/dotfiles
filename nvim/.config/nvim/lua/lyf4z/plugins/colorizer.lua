return { -- High-performance color highlighter
  "NvChad/nvim-colorizer.lua",
  event = "BufRead",
  config = function()
    -- Protected call
    local status_ok, colorizer = pcall(require, "colorizer")
    if not status_ok then
      return
    end

    colorizer.setup({
      filetypes = { "*" },
      names = false, -- "Name" codes like Blue oe blue
      RRGGBBAA = true, -- #RRGGBBAA hex codes
      rgb_fn = true, -- CSS rgb() and rgba() functions
      hsl_fn = true, -- CSS hsl() and hsla() functions
    })
  end,
}
