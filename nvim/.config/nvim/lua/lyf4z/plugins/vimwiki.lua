return {
  "vimwiki/vimwiki",
  branch = "dev",
  lazy = false,
  priority = 1000,
  init = function()
    vim.g.vimwiki_global_ext = 0
    vim.g.vimwiki_list = {
      {
        path = "~/repo/vimwiki",
        syntax = "markdown",
        ext = ".md",
      },
    }
  end,
}
