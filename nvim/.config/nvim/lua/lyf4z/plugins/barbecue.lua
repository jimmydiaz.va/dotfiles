return {
  -- A VS Code like winbar for Neovim
  "utilyre/barbecue.nvim",
  version = "*",
  dependencies = {
    "SmiteshP/nvim-navic",
  },
  config = function()
    local _barbecue = require("barbecue")

    _barbecue.setup() -- call defaults
  end,
}
