return {
  "folke/which-key.nvim",
  dependencies = {
    "echasnovski/mini.icons",
  },
  event = "VeryLazy",
  -- opts = {
  --   icons = {
  --     mappings = true, -- change to false to disable icons
  --   },
  -- },
  init = function()
    vim.o.timeout = true
    vim.o.timeoutlen = 500
  end,
  config = function()
    local wk = require("which-key")
    local mi = require("mini.icons")

    wk.setup({
      plugins = {
        marks = true,
        registers = true,
        spelling = {
          enabled = true,
          suggestions = 20,
        },
        presets = {
          operators = false,
          motions = false,
          text_objects = false,
          windows = false,
          nav = false,
          z = false,
          g = false,
        },
        show_help = false,
        show_keys = false,
      },
      icons = {
        group = "+", -- symbol prepended to a group
      },
    })

    mi.setup({
      -- add configuration here
    })

    wk.add({
      { "<leader>f", group = " File Search", icon = "   " }, -- group
      { "<leader>ff", "<cmd>Telescope find_files<cr>", desc = "Fuzzy find files in cwd", mode = "n" },
      { "<leader>fr", "<cmd>Telescope oldfiles<cr>", desc = "Fuzzy find recent files", mode = "n" },
      { "<leader>fl", "<cmd>Telescope live_grep<cr>", desc = "Fuzzy string in cwd", mode = "n" },
      { "<leader>fs", "<cmd>Telescope grep_string<cr>", desc = "Fuzzy string in cwd", mode = "n" },
      { "<leader>ft", "<cmd>TodoTelescope<cr>", desc = "Find todos", mode = "n" },
      { "<leader>fb", "<cmd>Telescope buffers<cr>", desc = "List open buffers", mode = "n" },
      { "<leader>fm", "<cmd>Telescope man_pages<cr>", icon = "  ", desc = "List manpage", mode = "n" },
      { "<leader>fc", "<cmd>Telescope colorscheme<cr>", desc = "List colorschemes", mode = "n" },
      { "<leader>fk", "<cmd>Telescope keymaps<cr>", desc = "List normal mode keymappings", mode = "n" },
      { "<leader>fn", "<cmd>Telescope neoclip<cr>", desc = "Show clipboard", mode = "n" },
      { "<leader>fz", "<cmd>Telescope zoxide list<cr>", desc = "Change directory", mode = "n" },

      { "<leader>s", group = " Windows", icon = "   " }, -- group
      { "<leader>sv", "<C-w>v", desc = "Split window vertically", mode = "n" },
      { "<leader>sh", "<C-w>s", desc = "Split window horizontally", mode = "n" },
      { "<leader>se", "<C-w>=", desc = "Make splits equal size", mode = "n" },
      { "<leader>sx", "<C-w>c", desc = "Close current split", mode = "n" },
      { "<leader>sm", "<cmd>MaximizerToggle<cr>", desc = "Maximize/Toggle splits", mode = "n" },

      { "<leader>o", group = "Writing", icon = " } " }, -- group
      { "<leader>ot", "<cmd>TwilightEnable<cr>", desc = "On twilight", mode = "n" },
      { "<leader>oT", "<cmd>TwilightDisable<cr>", desc = "Off twilight", mode = "n" },
      { "<leader>oz", "<cmd>ZenMode<cr>", desc = "Toggle Zenmode", mode = "n" },

      { "<leader>w", group = "Misc", icon = "  " }, -- group
      { "<leader>wr", "<cmd>SessionRestore<cr>", desc = "Restore session for cwd", mode = "n" },
      { "<leader>ws", "<cmd>SessionSave<cr>", desc = "Save session for auto root dir", mode = "n" },

      { "<leader>e", group = "Explorer", icon = "  " }, -- group
      { "<leader>ee", "<cmd>NvimTreeToggle<cr>", desc = "Toggle file explorer", mode = "n" },
      { "<leader>ef", "<cmd>NvimTreeFindFileToggle<cr>", desc = "Toggle file explorer on current file", mode = "n" },
      { "<leader>ec", "<cmd>NvimTreeCollapse<cr>", desc = "Collapse file explorer", mode = "n" },
      { "<leader>er", "<cmd>NvimTreeRefresh<cr>", desc = "Refresh file explorer", mode = "n" },

      { "<leader>r", group = "Replace", icon = " 󰬲 " }, -- group
      { "<leader>ro", "<cmd><lua require('substitute').operator()cr>", desc = "Substitute with motion", mode = "n" },
      { "<leader>rl", "<cmd>lua require('substitute').line()<cr>", desc = "Substitute line", mode = "n" },
      { "<leader>re", "<cmd>lua require('substitute').eol()<cr>", desc = "Substitute to end", mode = "n" },
      { "<leader>rv", "<cmd>lua require substitute.visual()<cr>", desc = "Substitute in visual mode", mode = "n" },

      { "<leader>c", group = "Trouble", icon = " 󰁨 " }, -- group
      { "<leader>cx", "<cmd>TroubleToggle<cr>", desc = "Open/close trouble list", mode = "n" },
      {
        "<leader>tw",
        "<cmd>TroubleToggle workspace_diagnostics<cr>",
        desc = "Open trouble workspace diagnostics",
        mode = "n",
      },
      { "<leader>td", "<cmd>Toggle document_diagnostics<cr>", desc = "Open trouble workspace diagnostics", mode = "n" },
      { "<leader>tq", "<cmd>Toggle quickfix<cr>", desc = "Open trouble quickfix list", mode = "n" },
      { "<leader>tl", "<cmd>Toggle loclist<cr>", desc = "Open trouble location list", mode = "n" },
      { "<leader>tt", "<cmd>TodoTrouble<cr>", desc = "Open todos in trouble", mode = "n" },

      { "<leader>t", group = "Terminal", icon = "  " }, -- group
      { "<leader>tl", "<cmd>lua _LAZYGIT_TOGGLE()<cr>", desc = "Launch Lazygit", mode = "n" },
      { "<leader>th", "<cmd>lua _HTOP_TOGGLE()<cr>", desc = "Launch Htop", mode = "n" },
      { "<leader>tp", "<cmd>lua _PYTHON_TOGGLE()<cr>", desc = "Launch Python", mode = "n" },
      -- NOTE:
      -- I can also include a function in the options
      -- Below is an example
      -- {
      --   "<leader>fb",
      --   function()
      --     print("hello")
      --   end,
      --   desc = "Foobar",
      -- },
      -- { "<leader>fn", desc = "New File" },
      { "<leader>f1", hidden = true }, -- hide this keymap
      -- { "<leader>w", proxy = "<c-w>", group = "windows" }, -- proxy to window mappings
      {
        "<leader>b",
        group = "buffers",
        expand = function()
          return require("which-key.extras").expand.buf()
        end,
      },
      {
        -- Nested mappings are allowed and can be added in any order
        -- Most attributes can be inherited or overridden on any level
        -- There's no limit to the depth of nesting
        mode = { "n", "v" }, -- NORMAL and VISUAL mode
        { "<leader>q", "<cmd>q<cr>", desc = "Quit" }, -- no need to specify mode since it's inherited
        { "<leader>Q", "<cmd>qa!<cr>", desc = "Quit All" }, -- no need to specify mode since it's inherited
        { "<leader>w", "<cmd>w<cr>", desc = "Write" },
        { "<leader>u", "<cmd>Lazy<cr>", desc = "󰚰 Lazy" },
        { "<leader>a", "<cmd>Alpha<cr>", desc = "󰨇 Dashboard" },
        { "<leader>A", "<cmd>ASToggle<cr>", desc = "Toggle auto-save" },
        { "<leader>m", "<cmd>Mason<cr>", desc = "󰝔 Mason" },
        { "<leader>x", "<cmd>source %<cr>", desc = "󰘬 Source" },
      },
    })
  end,
}
