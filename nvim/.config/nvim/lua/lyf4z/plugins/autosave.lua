return {
  "pocco81/auto-save.nvim",
  events = { "BufRead", "BufNewFile" },
  config = function()
    local autosave = require("auto-save")

    -- configure autosave
    autosave.setup({})
  end,
}
