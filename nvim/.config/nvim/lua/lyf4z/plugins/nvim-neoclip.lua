return {
  "AckslD/nvim-neoclip.lua",
  requires = {
    -- dependency required by nvim-neoclip
    { "nvim-telescope/telescope.nvim" },
  },
  config = function()
    local _neoclip = require("neoclip")

    _neoclip.setup()
  end,
}
