return {
  "beauwilliams/focus.nvim",
  config = function()
    -- Use protected call
    local status_ok, focus = pcall(require, "focus")
    if not status_ok then
      return
    end

    -- Auto-resizing focus splits
    focus.setup() -- call defaults
  end,
}
