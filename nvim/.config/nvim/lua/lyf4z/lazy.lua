local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- initialize where to save plugins
require("lazy").setup({ { import = "lyf4z.plugins" }, { import = "lyf4z.plugins.lsp" } }, {
  checker = {
    enabled = true, -- lazy will check if plugin updates are available
    notify = false,
  },
  change_detection = { -- will hide the notification for reloading plugins
    notify = false,
  },
})
