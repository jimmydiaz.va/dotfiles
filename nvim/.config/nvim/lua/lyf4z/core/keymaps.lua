vim.g.mapleader = " " -- set leader key

local keymap = vim.keymap.set --for conciseness
local opts = { noremap = true, silent = true }

keymap("i", "jj", "<ESC>", opts) -- Exit insertmode with jj

keymap("n", "<leader>nh", ":nohl<CR>", { desc = "Clear search highlights" })

-- increment/decrement numbers
keymap("n", "<leader>+", "<C-a>", { desc = "Increment number" })
keymap("n", "<leader>-", "<C-x>", { desc = "Decrement number" })

-- tabs management
keymap("n", "<leader>to", "<cmd>tabnew<CR>", { desc = "open new tab" })
keymap("n", "<leader>tx", "<cmd>tabclose<CR>", { desc = "close current tab" })
keymap("n", "<leader>tn", "<cmd>tabn<CR>", { desc = "goto next tab" })
keymap("n", "<leader>tp", "<cmd>tabp<CR>", { desc = "goto previous tab" })
keymap("n", "<leader>tf", "<cmd>tabnew %<CR>", { desc = "open current buffer in new tab" })

keymap("v", "<", "<gv", opts) -- indent forward
keymap("v", ">", ">gv", opts) -- indent backward

-- Map Ctrl-A to move the cursor to the Start of Line in insert mode
keymap("i", "<C-a>", "<Home>", opts)
-- Map Ctrl-E to move the cursor to the End of Line in insert mode
keymap("i", "<C-e>", "<End>", opts)

-- Duplicate the current line and paste below with Ctrl-Alt-J
keymap("n", "<C-M-j>", "yyp", opts)

-- Duplicate the current line and paste above with Ctrl-Alt-K
keymap("n", "<C-M-k>", "yyP", opts)
