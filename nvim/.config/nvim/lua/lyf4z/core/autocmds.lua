-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua
-- Add any additional autocmds here
--
-- automatically center search results
local set = vim.keymap.set
set("n", "n", "nzzzv")
set("n", "N", "Nzzzv")
set("c", "<CR>", function()
  return vim.fn.getcmdtype() == "/" and "<CR>zzzv" or "<CR>"
end, { expr = true })

-- This autocmd highlights the previously yanked text
-- by temporarily applying the "Visual" highlight group
-- for 200 milliseconds.
vim.api.nvim_create_autocmd({ "TextYankPost" }, {
  callback = function()
    vim.highlight.on_yank({ higroup = "Visual", timeout = 200 })
  end,
})

vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = {
    "Jaq",
    "qf",
    "help",
    "man",
    "lspinfo",
    "spectre_panel",
    "lir",
    "DressingSelect",
    "tsplayground",
    "Markdown",
  },
  callback = function()
    vim.cmd([[
      nnoremap <silent> <buffer> q :close<CR>
      nnoremap <silent> <buffer> <esc> :close<CR>
      set nobuflisted
    ]])
  end,
})

-- Automatically close tab/vim when nvim-tree is the last window in the tab
vim.cmd("autocmd BufEnter * ++nested if winnr('$') == 1 && bufname() == 'NvimTree_' . tabpagenr() | quit | endif")

-- Make sure Vim windows is resized
vim.api.nvim_create_autocmd({ "VimResized" }, {
  callback = function()
    vim.cmd("tabdo wincmd =")
  end,
})

-- Activate the specific settings for markdown files
vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = { "markdown" },
  callback = function()
    vim.opt_local.wrap = true
    vim.opt_local.spell = true
    vim.opt_local.linebreak = true
  end,
})

-- temporarily hiding the status line when the "AlphaReady" event
-- is triggered and restoring it when the buffer is unloaded
vim.api.nvim_create_autocmd({ "User" }, {
  pattern = { "AlphaReady" },
  callback = function()
    vim.cmd([[
      set laststatus=0 | autocmd BufUnload <buffer> set laststatus=3
    ]])
  end,
})

-- Provide mappings and settings for buffers without a specified filetype.
-- The mappings allow you to close the buffer with 'q',
-- and navigate through the buffer using Ctrl+J and Ctrl+K.
-- The nobuflisted option ensures that these buffers are not listed
-- in the buffer list.
local buf_ft = vim.bo.filetype
if buf_ft == "" or buf_ft == nil then
  vim.cmd([[
    nnoremap <silent> <buffer> q :close<CR>
    nnoremap <silent> <buffer> <c-j> j<CR>
    nnoremap <silent> <buffer> <c-k> k<CR>
    set nobuflisted
  ]])
end

-- This autocmd runs the wincmd = command for each tab.
-- The wincmd = command evenly distributes the windows
-- within each tab when the Vim window is resized.
vim.api.nvim_create_autocmd({ "VimResized" }, {
  callback = function()
    vim.cmd("tabdo wincmd =")
  end,
})

-- This autocmd simply quits Vim when entering the command-line window.
vim.api.nvim_create_autocmd({ "CmdWinEnter" }, {
  callback = function()
    vim.cmd("quit")
  end,
})

-- This autocmd removes the c, r, and o flags from the 'formatoptions' option,
-- which affects the automatic formatting behavior when editing text.
-- Removing these flags disables the automatic comment continuation
-- and automatic line wrapping for newly entered buffers
vim.api.nvim_create_autocmd({ "BufWinEnter" }, {
  callback = function()
    vim.cmd("set formatoptions-=cro")
  end,
})

-- This autocmd sets up a highlight link where
-- the illuminatedWord highlight group is linked to the
-- LspReferenceText highlight group. This can be useful
-- for code highlighting or customization
-- related to language server references.
vim.api.nvim_create_autocmd({ "VimEnter" }, {
  callback = function()
    vim.cmd("hi link illuminatedWord LspReferenceText")
  end,
})

-- This autocmd runs the checktime command
-- when entering any buffer. The checktime command checks
-- if the file has been modified outside of Vim
-- and updates the buffer if necessary.
vim.api.nvim_create_autocmd({ "BufWinEnter" }, {
  pattern = { "*" },
  callback = function()
    vim.cmd("checktime")
  end,
})

-- This autocmd checks if the LuaSnip plugin is installed.
-- If it is, and if there is an expandable snippet
-- or jumpable position, it unlinks the current snippet.
vim.api.nvim_create_autocmd({ "CursorHold" }, {
  callback = function()
    local status_ok, luasnip = pcall(require, "luasnip")
    if not status_ok then
      return
    end
    if luasnip.expand_or_jumpable() then
      vim.cmd([[silent! lua require("luasnip").unlink_current()]])
    end
  end,
})

-- will trigger linting according to the configured linter for each filetype
-- # local lint = require("lint")
-- # local lint_augroup = vim.api.nvim_create_augroup("lint", { clear = true })
-- #
-- # vim.api.nvim_create_autocmd({ "BufEnter", "BufWritePost", "InsertLeave" }, {
-- #   group = lint_augroup,
-- #   callback = function()
-- #     lint.try_lint()
-- #   end,
-- # })

vim.keymap.set("n", "<leader>l", function()
  lint.try_lint()
end, { desc = "Trigger linting for current file" })
