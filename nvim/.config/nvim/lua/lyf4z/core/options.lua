-- make netrw explore show tree
vim.cmd("let g:netrw_liststyle = 3")

local opt = vim.opt

opt.relativenumber = true
opt.number = true

-- tabs and idnentation
opt.tabstop = 2 -- 2 spaces for tabs (prettier default)
opt.shiftwidth = 2 -- spaces for indent width --NOTE: change to 4 if python
opt.expandtab = true -- expand tab to spaces
opt.autoindent = true -- copy indent from current line when starting new one

opt.wrap = false -- disable wrap

-- search settings
opt.ignorecase = true -- ignore case when searchingy
opt.smartcase = true -- if you include mix case in your search, assumes you want case-sensitive

opt.cursorline = true -- shows a cursor line

-- turn on termguicolors for tokyonight colorscheme to work
-- (have to use iterm2 or any other color true terminal)
opt.termguicolors = true
opt.background = "dark" -- colorschemes that can be light or dark will be made dark
opt.signcolumn = "yes" -- show sign column so that text doesn't shift

-- backspace
opt.backspace = "indent,eol,start" -- allow backspace on indent, end of line or insert mode start position

-- clipboard
opt.clipboard:append("unnamedplus") -- use system clipboard as default register

-- split windows
opt.splitright = true -- split vertical window to the right
opt.splitbelow = true -- split horizontal window to the bottom
