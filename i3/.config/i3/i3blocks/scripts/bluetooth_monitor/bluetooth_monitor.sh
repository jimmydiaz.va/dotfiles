#!/bin/bash

# Get Bluetooth power status
bluetooth_status=$(bluetoothctl show | grep "Powered" | awk '{print $2}')
connected_devices=$(bluetoothctl info | grep "Connected: yes" | wc -l)

if [[ "$bluetooth_status" == "no" ]]; then
    # Bluetooth is off
    echo "<span color='#FF0000'>󰂲</span> Off"
elif [[ "$bluetooth_status" == "yes" && "$connected_devices" -eq 0 ]]; then
    # Bluetooth is on but no devices are connected
    echo "<span color='#FFFF00'>󰂯</span> Not Connected"
elif [[ "$bluetooth_status" == "yes" && "$connected_devices" -gt 0 ]]; then
    # Fetch the name of the connected device(s)
    device_info=$(bluetoothctl devices Connected | awk '{$1=""; $2=""; print $0}' | xargs)
    
    # Fetch the battery level in parentheses (decimal format)
    battery_level=$(bluetoothctl info | grep "Battery Percentage" | grep -oP '\(\K[0-9]+(?=\))')
    
    # Default battery level if unavailable
    if [[ -z "$battery_level" ]]; then
        battery_level="N/A"
    fi

    # Color the device name (light green) and battery percentage (sky blue for 100%)
    device_info="<span color='#32CD32'>$device_info</span>"  # Light Green for device name
    if [[ "$battery_level" == "100" ]]; then
        battery_level="<span color='#00BFFF'>$battery_level%</span>"  # Sky Blue for 100%
    else
        battery_level="$battery_level%"  # No color for other percentages
    fi

    # Display connected devices in the format: Icon Device Name Battery%
    echo "<span color='#00FF00'>󰂰</span> $device_info $battery_level"
fi
