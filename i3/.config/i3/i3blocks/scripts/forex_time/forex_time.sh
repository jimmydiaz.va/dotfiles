#!/bin/bash

# Get the current time in Manila (for reference)
hour=$(TZ="Asia/Manila" date +'%H')
minute=$(TZ="Asia/Manila" date +'%M')

# Determine colors for Tokyo
if [ "$1" == "Tokyo" ]; then
    if [ "$hour" -ge 7 ] && [ "$hour" -lt 15 ]; then
        echo -e "Tokyo\nTokyo\n#00FF00"  # Green
    else
        echo -e "Tokyo\nTokyo\n#808080"  # Grey
    fi
fi

# Determine colors for New York
if [ "$1" == "NewYork" ]; then
    if [ "$hour" -ge 21 ] || [ "$hour" -lt 5 ]; then
        echo -e "New York\nNew York\n#00FF00"  # Green
    else
        echo -e "New York\nNew York\n#808080"  # Grey
    fi
fi

# Determine colors for London
if [ "$1" == "London" ]; then
    if [ "$hour" -ge 16 ] && [ "$hour" -lt 24 ]; then
        echo -e "London\nLondon\n#00FF00"  # Green
    else
        echo -e "London\nLondon\n#808080"  # Grey
    fi
fi
