#!/bin/bash

# Define color codes
RED='\033[0;31m'    # Red text
BOLD='\033[1m'      # Bold text
NC='\033[0m'        # No color

# Print the header in bold red 
echo -e "${BOLD}${RED}--- MOD KEYBINDINGS ---${NC}"

# Extract keybindings and actions from the spectrwm.conf file
grep MOD ~/.config/spectrwm/spectrwm.conf | awk -F '[=#]' '{print $2 "---" $3}' 

# Print the header in bold red 
echo -e "${BOLD}${RED}--- CONTROL KEYBINDINGS ---${NC}"

grep Control ~/.config/spectrwm/spectrwm.conf | awk -F '[=#]' '{print $2 "---" $3}' 

# Add a pause mechanism
echo -e "\nPress Enter to exit..."
read -r  # Wait for the user to press Enter before exiting
