#!/usr/bin/env bash

# Created By: Jake@Linux
# Created On: Sat 03 Sep 2022 03:56:04 PM CDT
# Project: bspwm scratchpad

winclass="$(xdotool search --class sckeymap)";
if [ -z "$winclass" ]; then
    alacritty --class sckeymap -e ~/.config/spectrwm/scripts/keymap.sh 
else
    if [ ! -f /tmp/sckeymap ]; then
        touch /tmp/sckeymap && xdo hide "$winclass"
    elif [ -f /tmp/sckeymap ]; then
        rm /tmp/sckeymap && xdo show "$winclass"
    fi
fi
