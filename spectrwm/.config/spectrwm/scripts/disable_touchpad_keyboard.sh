#!/bin/bash

# Define ANSI escape codes for colors
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No color (reset)

# Names of the devices
EXTERNAL_KEYBOARD="SEMICO USB Keyboard Consumer Control"
BUILT_IN_KEYBOARD="AT Translated Set 2 keyboard"
TOUCHPAD="ELAN1300:00 04F3:3057 Touchpad"

LOCK_FILE="/tmp/keyboard_touchpad_toggle.lock"

# If the lock file exists, the script is running
if [ -f "$LOCK_FILE" ]; then
    echo "Script is already running!"
    exit 1
fi

# Create lock file to indicate the script is running
touch "$LOCK_FILE"

# Function to clean up lock file when the script exits
cleanup() {
    rm -f "$LOCK_FILE"
}
trap cleanup EXIT

# Check the current status of the built-in keyboard and touchpad
BUILT_IN_KEYBOARD_STATUS=$(xinput list-props "$(xinput list --id-only "$BUILT_IN_KEYBOARD")" | grep "Device Enabled" | awk '{print $4}')
TOUCHPAD_STATUS=$(xinput list-props "$(xinput list --id-only "$TOUCHPAD")" | grep "Device Enabled" | awk '{print $4}')

# Check if the external keyboard is connected
if xinput list | grep -q "$EXTERNAL_KEYBOARD"; then
    # Check if the built-in keyboard and touchpad are already disabled
    if [[ "$BUILT_IN_KEYBOARD_STATUS" -eq 0 && "$TOUCHPAD_STATUS" -eq 0 ]]; then
        echo -e "${RED}  Built-in keyboard and touchpad are already disabled.${NC}"
    else
        # Disable built-in keyboard and touchpad
        xinput disable "$(xinput list --id-only "$BUILT_IN_KEYBOARD")"
        xinput disable "$(xinput list --id-only "$TOUCHPAD")"
        echo -e "${RED}  Disabled${NC}" # Red color for disabled
    fi
else
    # Check if the built-in keyboard and touchpad are already enabled
    if [[ "$BUILT_IN_KEYBOARD_STATUS" -eq 1 && "$TOUCHPAD_STATUS" -eq 1 ]]; then
        echo -e "${GREEN}  Built-in keyboard and touchpad are already enabled.${NC}"
    else
        # Enable built-in keyboard and touchpad
        xinput enable "$(xinput list --id-only "$BUILT_IN_KEYBOARD")"
        xinput enable "$(xinput list --id-only "$TOUCHPAD")"
        echo -e "${GREEN}  Enabled${NC}" # Green color for enabled
    fi
fi
