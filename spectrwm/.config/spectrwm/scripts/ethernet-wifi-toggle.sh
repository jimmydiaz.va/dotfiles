#!/bin/bash 

# Get the status of ethernet and WiFi using nmcli 
ethernet_status=$(nmcli device status | grep ethernet | awk '{print $3}')
wifi_status=$(nmcli radio wifi)

# If ethernet is enabled turn off WiFi
if [ "$ethernet_status" == "connected" ]; then
    if [ "$wifi_status" == "enabled" ]; then 
        nmcli radio wifi off 
        echo "WiFi is disabled since Ethernet is connected" 
    fi
else
    # if ethernet is not connected 
    if [ "$wifi_status" == "disabled" ]; then 
        nmcli radio wifi on 
        echo "WiFi enable since Ethernet is disconnected"
    fi 
fi
