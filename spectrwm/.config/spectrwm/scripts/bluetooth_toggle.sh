#!/bin/bash

# Get the current power state of Bluetooth
status=$(bluetoothctl show | grep "Powered" | awk '{print $2}')

if [ "$status" = "yes" ]; then
    sleep 2
    # Get the connected device MAC address
    connected_device=$(bluetoothctl devices Connected | awk '{print $2}')
    
    # Check if no MAC address is detected (no connected device)
    if [ -z "$connected_device" ]; then
        # If no device is connected, run the connection script
        sh ~/.config/spectrwm/scripts/connect_my_bluetooth.sh
        notify-send "Bluetooth turned on and connected"
    else
        # If a device is already connected, disconnect it 
        bluetoothctl disconnect
    fi
else
    # If Bluetooth is off, power it on and run the connection script
    bluetoothctl power on
    sh ~/.config/spectrwm/scripts/connect_my_bluetooth.sh
    notify-send "Bluetooth turned on and connected"
fi
