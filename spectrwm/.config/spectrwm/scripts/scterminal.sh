#!/usr/bin/env bash

# Created By: Jake@Linux
# Created On: Sat 03 Sep 2022 03:56:04 PM CDT
# Project: bspwm scratchpad

winclass="$(xdotool search --class scterminal)";
if [ -z "$winclass" ]; then
    alacritty --class scterminal 
else
    if [ ! -f /tmp/scterminal ]; then
        touch /tmp/scterminal && xdo hide "$winclass"
    elif [ -f /tmp/scterminal ]; then
        rm /tmp/scterminal && xdo show "$winclass"
    fi
fi
