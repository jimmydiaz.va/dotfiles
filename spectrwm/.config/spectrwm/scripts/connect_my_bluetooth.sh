#!/bin/bash

# Script: Bluetooth Auto Connect (Alias Detection)
# Author: lyf4z
# Description: This script first attempts to connect to known Bluetooth devices by their alias.
#              If the alias method fails, it falls back to stored MAC addresses.

# Array of device aliases to search for
DEVICE_ALIASES=("Bluetooth Music" "OWS")

# Array of known MAC addresses for fallback
KNOWN_MACS=("41:42:DC:2B:E8:CE")  # Add more MAC addresses as needed

# Function to get the MAC address of a device by its alias
get_mac_by_alias() {
    local alias=$1
    bluetoothctl devices | grep "$alias" | awk '{print $2}'
}

# Function to connect to a device by its MAC address
connect_to_device() {
    local mac=$1
    echo "Connecting to device with MAC address $mac..."
    bluetoothctl connect "$mac"
    
    # Check if the connection was successful
    if bluetoothctl info "$mac" | grep -q "Connected: yes"; then
        echo "Successfully connected to the device."
        exit 0  # Exit after successful connection
    else
        echo "Failed to connect to the device."
    fi
}

# Try connecting by alias first
echo "Starting Bluetooth scan and looking for known devices by alias..."
bluetoothctl scan on &

# Capture the process ID of the bluetoothctl scan
SCAN_PID=$!

# Monitor the scan output for matching device aliases
while IFS= read -r line; do
    # Loop through known device aliases
    for DEVICE_ALIAS in "${DEVICE_ALIASES[@]}"; do
        if echo "$line" | grep -q "$DEVICE_ALIAS"; then
            echo "Detected known device: $DEVICE_ALIAS"
            
            # Get the MAC address of the detected device by alias
            DEVICE_MAC=$(get_mac_by_alias "$DEVICE_ALIAS")
            
            if [ -n "$DEVICE_MAC" ]; then
                # Stop the Bluetooth scan
                echo "Stopping Bluetooth scan..."
                kill "$SCAN_PID"
                bluetoothctl scan off
                
                # Try connecting using the alias-based MAC address
                connect_to_device "$DEVICE_MAC"
            else
                echo "Could not find MAC address for alias: $DEVICE_ALIAS"
            fi
        fi
    done
done < <(bluetoothctl devices)

# Alias connection failed, now attempt to connect via stored MAC addresses
echo "Attempting to connect using stored MAC addresses..."
for MAC in "${KNOWN_MACS[@]}"; do
    connect_to_device "$MAC"
done

# If no connection succeeded, stop scanning and exit with failure
bluetoothctl scan off
echo "No known devices found or failed to connect. Exiting."
exit 1
