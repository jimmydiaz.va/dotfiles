#!/usr/bin/env bash

# Created By: Jake@Linux
# Created On: Sat 03 Sep 2022 03:56:04 PM CDT
# Project: bspwm scratchpad

winclass="$(xdotool search --class scmusic)";
if [ -z "$winclass" ]; then
    alacritty --class scmusic -e ncmpcpp
else
    if [ ! -f /tmp/scmusic ]; then
        touch /tmp/scmusic && xdo hide "$winclass"
    elif [ -f /tmp/scmusic ]; then
        rm /tmp/scmusic && xdo show "$winclass"
    fi
fi
