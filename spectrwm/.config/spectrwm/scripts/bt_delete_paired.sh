#!/bin/bash

# Get the MAC addresses of all paired devices
paired_devices=$(bluetoothctl devices | awk '{print $2}')

# Loop through each paired device and remove it
for device in $paired_devices; do
    bluetoothctl remove "$device"
    echo "Removed device: $device"
done

echo "All paired devices have been removed."
