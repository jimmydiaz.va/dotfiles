#!/bin/bash

# Get the number of available updates
updates=$(checkupdates 2>/dev/null | wc -l)

# Show the number of updates in Polybar
if [ "$1" == "show" ]; then
    # Open Alacritty to run `yay -Syu` for updating
    alacritty -e bash -c "sudo reflector --latest 50 --number 20 --sort age --protocol https --save /etc/pacman.d/mirrorlist && \
      sudo pacman -Sy && \
      sudo pacman -Syyu && \
      paru -Sua --noconfirm && \
      paru -Syu --noconfirm && \
      sudo pacman -Sc && \
      sudo pacman -Scc && \
      sudo rm -rfv ~/.cache/* && \
      sudo journalctl --vacuum-size=50M && \
      paccache -r && \
      sudo pacman -R $(pacman -Qtdq) && \
      sudo rm /var/lib/pacman/db.lck && \
      sudo pacman -Rns \$(pacman -Qtdq) && \
      sudo pacman -Rns \$(pacman -Qqdt) && \
      echo "Update complete. Exiting..." && \
      sleep 2 && exit
    "
fi

# Output the number of updates to Polybar
echo " $updates"
