#!/bin/bash

# Check if calcurse is installed
if ! command -v calcurse &> /dev/null; then
    echo "Calcurse is not installed. Please install it first."
    exit 1
fi

# Launch calcurse with the default interface
sh ~/.config/spectrwm/scripts/sccalendar.sh

# If you want to launch calcurse with a specific option, uncomment one of the lines below:
# Launch calcurse showing appointments for today
# calcurse -d

# Launch calcurse showing upcoming appointments
# calcurse -a
