#!/usr/bin/env bash

# Created By: Jake@Linux
# Created On: Sat 03 Sep 2022 03:56:04 PM CDT
# Project: bspwm scratchpad

winclass="$(xdotool search --class scpulsemixer)";
if [ -z "$winclass" ]; then
    alacritty --class scpulsemixer -e pulsemixer
else
    if [ ! -f /tmp/scpulsemixer ]; then
        touch /tmp/scpulsemixer && xdo hide "$winclass"
    elif [ -f /tmp/scpulsemixer ]; then
        rm /tmp/scpulsemixer && xdo show "$winclass"
    fi
fi
