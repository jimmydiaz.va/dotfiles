#!/usr/bin/env bash

# Created By: Jake@Linux
# Created On: Sat 03 Sep 2022 03:56:04 PM CDT
# Project: spectrwm scratchpad

winclass="$(xdotool search --class sccalendar)";
if [ -z "$winclass" ]; then
    alacritty --class sccalendar -e calcurse
else
    if [ ! -f /tmp/sccalendar ]; then
        touch /tmp/sccalendar && xdo hide "$winclass"
    elif [ -f /tmp/sccalendar ]; then
        rm /tmp/sccalendar && xdo show "$winclass"
    fi
fi
