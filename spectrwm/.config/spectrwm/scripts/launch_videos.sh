#!/bin/bash

# Directory containing your videos
video_directory="/mnt/gamma/videos/programming/python/rooney/"

# Find all video files recursively and list them
videos=$(find "$video_directory" -type f \( \
    -iname "*.mp4" -o \
    -iname "*.mkv" -o \
    -iname "*.avi" -o \
    -iname "*.mov" -o \
    -iname "*.flv" -o \
    -iname "*.wmv" -o \
    -iname "*.webm" -o \
    -iname "*.mpeg" -o \
    -iname "*.mpg" -o \
    -iname "*.3gp" -o \
    -iname "*.ogv" -o \
    -iname "*.m4v" -o \
    -iname "*.h265" -o \
    -iname "*.hevc" \
\))

# Extract only the base names (file names without full path)
video_names=$(echo "$videos" | xargs -I {} basename {})

# Display base names in rofi and get selected video
selected_video=$(echo "$video_names" | rofi -dmenu -i -p "Select a video:")

# Match selected video with full path (case insensitive)
full_path=$(echo "$videos" | grep -i -F "/$selected_video")

# If a video is selected, play it with mpv
if [ -n "$full_path" ]; then
    mpv "$full_path" &
else
    notify-send "No video selected or video not found!"
fi
