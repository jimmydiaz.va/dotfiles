#!/bin/bash

# List VirtualBox VMs and pass them to Rofi
vms=$(VBoxManage list vms | awk -F '"' '{print $2}')  # Extract VM names
selected_vm=$(echo "$vms" | rofi -dmenu -p "Select a VM to launch:")

if [ -n "$selected_vm" ]; then
    VBoxManage startvm "$selected_vm" --type headless  # Start VM in headless mode
fi
