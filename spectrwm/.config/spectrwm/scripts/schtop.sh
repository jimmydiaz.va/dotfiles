#!/usr/bin/env bash

# Created By: Jake@Linux
# Created On: Sat 03 Sep 2022 03:56:04 PM CDT
# Project: bspwm scratchpad

winclass="$(xdotool search --class schtop)";
if [ -z "$winclass" ]; then
    alacritty --class schtop -e htop
else
    if [ ! -f /tmp/schtop ]; then
        touch /tmp/schtop && xdo hide "$winclass"
    elif [ -f /tmp/schtop ]; then
        rm /tmp/schtop && xdo show "$winclass"
    fi
fi
