#!/usr/bin/env python3

import os
import subprocess

# Define the directory containing your videos
video_directory = os.path.expanduser("/mnt/gamma/videos/")

# Supported video formats
video_extensions = (
    ".mp4",
    ".mkv",
    ".avi",
    ".mov",
    ".flv",
    ".wmv",
    ".webm",
    ".mpeg",
    ".mpg",
    ".3gp",
    ".ogv",
    ".m4v",
    ".h265",
    ".hevc",
)

# Recursively find all video files
video_files = []
for root, dirs, files in os.walk(video_directory):
    for file in files:
        if file.lower().endswith(video_extensions):
            video_files.append(os.path.join(root, file))

# Extract only the base names (file names without full path)
video_names = [os.path.basename(video) for video in video_files]

# Call rofi to select a video file
if video_names:
    result = subprocess.run(
        ["rofi", "-dmenu", "-i", "-p", "Select a video:"],
        input="\n".join(video_names),
        text=True,
        capture_output=True,
    )
    selected_video = result.stdout.strip()

    # Match selected video with full path
    for video in video_files:
        if os.path.basename(video).lower() == selected_video.lower():
            # Play the selected video using mpv
            subprocess.run(["mpv", video])
            break
else:
    print("No videos found!")
